# Lintel Dark

A dark color scheme for [chriskempson/base16](https://github.com/chriskempson/base16) inspired by [ddnexus/equilux-theme](https://github.com/ddnexus/equilux-theme) and [morhetz/gruvbox](https://github.com/morhetz/gruvbox).
